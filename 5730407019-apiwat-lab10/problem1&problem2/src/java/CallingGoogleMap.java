/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author nooknik
 */
@WebServlet(urlPatterns = {"/CallingGoogleMap"})
public class CallingGoogleMap extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           out.println("<h1>Servlet CallingGoogleMap at " + request.getContextPath() + "</h1>");
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");

            String q = URLEncoder.encode(request.getParameter("address"), "UTF-8");
            String URL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + q + "&sensor=false&language=en&key=AIzaSyBXdd94g9MIyw9MUZKLwp3YaXPyJxwXMV8";
            JSONParser reader = new JSONParser();
            String jsonString = callAPI(URL);


            try {
                Object obj = reader.parse(jsonString);
                JSONObject person = (JSONObject) obj;
                JSONArray results = (JSONArray) person.get("results");
                JSONObject jsonValue = (JSONObject) reader.parse(results.get(0).toString());
                JSONObject Geo = (JSONObject) jsonValue.get("geometry");
                JSONObject location = (JSONObject) Geo.get("location");

                out.println(location.get("lat") + " " + location.get("lng"));

                response.sendRedirect("https://www.google.co.th/maps/search/"
                        + location.get("lat") + "," + location.get("lng"));
                
            } catch (ParseException ex) {
                out.println(ex.getPosition());
                out.println(ex);
            }

            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String callAPI(String API) {

      StringBuilder sb = new StringBuilder();
        URLConnection conn = null;
        InputStreamReader in = null;
        try {
            URL url = new URL(API);
            conn = url.openConnection();
            in = new InputStreamReader(conn.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(in);

            int cp;

            while ((cp = bufferedReader.read()) != -1) {
                sb.append((char) cp);
            }
            bufferedReader.close();

            in.close();
        } catch (Exception e) {

        }

        return sb.toString();
    }

   

}
