

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


@WebServlet(urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
                  
            JSONArray root = new JSONArray();
            
            JSONObject one = new JSONObject();
            JSONObject author1 = new JSONObject();
            
            JSONObject two = new JSONObject();
            JSONObject author2 = new JSONObject();
            
            author1.put("petition", "พร พิรุณ");
            author1.put("meloday", "เอื้อ สุนทรสนาน");            
            one.put("author", author1);
            one.put("title", "มาร์ช ม.ข.");
            one.put("content", "มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา");

            author2.put("petition", "สุนทราภรณ์");
            author2.put("meloday", "สุนทราภรณ์");            
            two.put("author", author2);
            two.put("title", "เสียงสนครวญ");
            two.put("content", "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว");
           
            root.add(one);
            root.add(two);          
            out.print(root);                 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
